#pragma once
#include "TypeInformation.h"
#include <string>
#include <vector>

using namespace std;

class EnumType :
	public TypeInformation
{
public:
	EnumType();
	EnumType(vector<string> names);
	EnumType(string firstName);
	~EnumType();
	void AddEnum(string name);
	vector<string> getEnums();
private:
	vector<string> m_names;
};

