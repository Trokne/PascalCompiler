#pragma once
#include <string>

using namespace std;

class UseMethod
{
public:
	enum UseCode {
		Progs = 300,		// ���������
		Types = 301,		// ���
		Consts = 302,		// ���������
		Vars = 303,			// ����������
		Procs = 304,		// ���������
		Funcs = 305,		// �������
		ProcsParam = 306	// �������� ���������
	};
	int getUseCode();
protected:
	int m_useCode;
};

