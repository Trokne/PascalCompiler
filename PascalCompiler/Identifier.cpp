#include "stdafx.h"
#include "Identifier.h"


Identifier::Identifier(string name, UseMethod * useMethod)
{
	m_name = name;
	m_useMethod = useMethod;
}

Identifier::Identifier(string name, UseMethod * useMethod, TypeInformation * typeInfo)
{
	m_name = name;
	m_useMethod = useMethod;
	m_typeInfo = typeInfo;
}

void Identifier::setTypeInfo(TypeInformation * typeInfo)
{
	m_typeInfo = typeInfo;
}

TypeInformation * Identifier::getTypeInfo()
{
	return m_typeInfo;
}

string Identifier::getName()
{
	return m_name;
}

UseMethod * Identifier::getUseMethod()
{
	return m_useMethod;
}
