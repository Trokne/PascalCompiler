#include "stdafx.h"
#include "ProcsIdentifier.h"


ProcsIdentifier::ProcsIdentifier()
{
	m_useCode = UseCode::Procs;
}

void ProcsIdentifier::addProcParam(Identifier * param)
{
	m_params.push_back(param);
}

Identifier * ProcsIdentifier::getParam(int index)
{
	if (index >= m_params.size())
		return nullptr;
	return m_params.at(index);
}

size_t ProcsIdentifier::getParamsCount()
{
	return m_params.size();
}
