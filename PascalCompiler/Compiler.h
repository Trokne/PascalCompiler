#pragma once
#include "SynSemAnalizer.h"

class Compiler
{
public:
	Compiler(string, bool);
	void Compile();
private:
	Lexer* m_lexer;
	SynSemAnalizer* m_synAnalizer;
	bool m_isFolder;
	string m_fileName;
};

