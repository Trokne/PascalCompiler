#include "stdafx.h"
#include "ProcParam.h"


ProcParam::ProcParam(bool isByRef)
{
	m_transferType = isByRef ? TransferType::ByReference : TransferType::ByValue;
	m_useCode = UseCode::ProcsParam;
}


ProcParam::~ProcParam()
{
}
