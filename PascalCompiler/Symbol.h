#pragma once
#include <limits>
#include <string>
#include "SymType.h"

using namespace std;

class Symbol
{
public:
	Symbol(SymType symType);
	SymType getType();
	string getName();
	void setType(SymType symType);
	void setValue(int value);
	void setValue(float value);
	void setValue(string value);
	void setName(string name);
	string getCharValue();
private:
	SymType m_type;
	int m_nmbInt;
	float m_nmbFloat;
	string m_constStr;
	string m_name;
};

