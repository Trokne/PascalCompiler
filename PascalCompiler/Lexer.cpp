#include "stdafx.h"
#include "Lexer.h"


Lexer::Lexer(fs::path filePath)
{
	isEnd = false;
	m_ioModule = new InputOutput(filePath);
	generateKeyWordsMap();
}

Lexer::~Lexer()
{
	delete m_curSymbol;
	delete m_ioModule;
}

void Lexer::printProgram(bool isFolder)
{
	m_ioModule->printProgram(isFolder);
}

void Lexer::nextSymbol()
{
	while (m_ioModule->getChar() == ' ' || m_ioModule->getChar() == '\t')
		m_ioModule->nextChar();
	if (m_ioModule->getChar() == '\0')
	{
		isEnd = true;
		m_curSymbol->setType(emptyFile);
		return;
	}	m_symbolPos.setCharNumber(m_ioModule->getCharPosition()->getCharNumber());
	m_symbolPos.setLineNumber(m_ioModule->getCharPosition()->getLineNumber());
	scanSymbol();
}

void Lexer::addError(int errorCode, TextPosition position)
{
	m_ioModule->addError(errorCode, position);
}

void Lexer::addError(int errorCode)
{
	m_ioModule->addError(errorCode, m_symbolPos);
}

Symbol * Lexer::getCurrentSymbol()
{
	return m_curSymbol;
}

TextPosition Lexer::getCurrentPosition()
{
	return m_symbolPos;
}

bool Lexer::addToNameTable(string name)
{
	return (m_nameTable.insert(name)).second;
}

bool Lexer::isContainInNameTable(string name)
{
	return m_nameTable.find(name) != m_nameTable.end();
}

void Lexer::setProgramName(string name)
{
	m_ioModule->setProgramName(name);
}

void Lexer::generateKeyWordsMap()
{
	m_keyWords = {
		{ "if", SymType::ifsy },
		{ "do", SymType::dosy },
		{ "of", SymType::ofsy },
		{ "or", SymType::orsy },
		{ "in", SymType::insy },
		{ "to", SymType::tosy },
		{ "end", SymType::endsy },
		{ "var", SymType::varsy },
		{ "div", SymType::divsy },
		{ "and", SymType::andsy },
		{ "not", SymType::notsy },
		{ "for", SymType::forsy },
		{ "mod", SymType::modsy },
		{ "nil", SymType::nilsy },
		{ "set", SymType::setsy },
		{ "then", SymType::thensy },
		{ "else", SymType::elsesy },
		{ "case", SymType::casesy },
		{ "file", SymType::filesy },
		{ "goto", SymType::gotosy },
		{ "type", SymType::typesy },
		{ "with", SymType::withsy },
		{ "begin", SymType::beginsy },
		{ "while", SymType::whilesy },
		{ "array", SymType::arraysy },
		{ "const", SymType::constsy },
		{ "label", SymType::labelsy },
		{ "until", SymType::untilsy },
		{ "downto", SymType::downtosy },
		{ "packed", SymType::packedsy },
		{ "record", SymType::recordsy },
		{ "repeat", SymType::repeatsy },
		{ "program", SymType::programsy },
		{ "function", SymType::functionsy },
		{ "procedure", SymType::proceduresy }
	};
}

void Lexer::scanSymbol()
{
	if (m_ioModule->getChar() < -1
		|| m_ioModule->getChar() > 255)
	{
		m_ioModule->nextChar();
		addError(6, getCurrentPosition());
		nextSymbol();
	}
	if (isEnglishLetter(m_ioModule->getChar()))
	{
		scanIdentOrKeyword();
		return;
	}
	if (m_ioModule->getChar() >= -1
		&& m_ioModule->getChar() <= 255
		&& isdigit(m_ioModule->getChar()))
	{
		scanNumericConst();
		return;
	}
	if (m_ioModule->getChar() == '\'')
	{
		scanCharConst();
		return;
	}
	switch (m_ioModule->getChar())
	{
		case '\n':
			m_ioModule->nextChar();
			nextSymbol();
			break;
		case '<':
			m_ioModule->nextChar();
			if (m_ioModule->getChar() == '=')
			{
				delete m_curSymbol;
				m_curSymbol = new Symbol(SymType::laterequal);
				m_ioModule->nextChar();
			}
			else if (m_ioModule->getChar() == '>')
			{
				delete m_curSymbol;
				m_curSymbol = new Symbol(SymType::latergreater);
				m_ioModule->nextChar();					
			}
			else
			{
				delete m_curSymbol;
				m_curSymbol = new Symbol(SymType::later);
			}
			break;
		case '>':
			m_ioModule->nextChar();
			if (m_ioModule->getChar() == '=')
			{
				delete m_curSymbol;
				m_curSymbol = new Symbol(SymType::greaterequal);
				m_ioModule->nextChar();
			}
			else
			{
				delete m_curSymbol;
				m_curSymbol = new Symbol(SymType::greater);
			}
			break;
		case ':':
			m_ioModule->nextChar();
			if (m_ioModule->getChar() == '=')
			{
				delete m_curSymbol;
				m_curSymbol = new Symbol(SymType::assign);
				m_ioModule->nextChar();
			}
			else 
			{
				delete m_curSymbol;
				m_curSymbol = new Symbol(SymType::colon);
			}
			break;
		case '+':
			delete m_curSymbol;
			m_curSymbol = new Symbol(SymType::plus);
			m_ioModule->nextChar();
			break;
		case '-':
			delete m_curSymbol;
			m_curSymbol = new Symbol(SymType::minus);
			m_ioModule->nextChar();
			break;
		case '*':
			m_ioModule->nextChar();
			if (m_ioModule->getChar() == ')')
			{
				delete m_curSymbol;
				m_curSymbol = new Symbol(SymType::rcomment);
				m_ioModule->nextChar();
			}
			else
			{
				delete m_curSymbol;
				m_curSymbol = new Symbol(SymType::star);
			}
			break;
		case '/':
			m_ioModule->nextChar();
			if (m_ioModule->getChar() == '/')
				scanOneLineComment();
			else
			{
				delete m_curSymbol;
				m_curSymbol = new Symbol(SymType::slash);
			}
			break;
		case '=':
			delete m_curSymbol;
			m_curSymbol = new Symbol(SymType::equal);
			m_ioModule->nextChar();
			break;
		case '(':
			m_ioModule->nextChar();
			if (m_ioModule->getChar() == '*')
				scanMultilineComment();
			else 
			{
				delete m_curSymbol;
				m_curSymbol = new Symbol(SymType::leftpar);
			}
			break;
		case ')':
			delete m_curSymbol;
			m_curSymbol = new Symbol(SymType::rightpar);
			m_ioModule->nextChar();
			break;
		case '{':
			m_ioModule->nextChar();
			scanMultilineComment();
			break;
		case '}':
			delete m_curSymbol;
			m_curSymbol = new Symbol(SymType::frpar);
			m_ioModule->nextChar();
			break;
		case '[':
			delete m_curSymbol;
			m_curSymbol = new Symbol(SymType::lbracket);
			m_ioModule->nextChar();
			break;
		case ']':
			delete m_curSymbol;
			m_curSymbol = new Symbol(SymType::rbracket);
			m_ioModule->nextChar();
			break;
		case '.':
			m_ioModule->nextChar();
			if (m_ioModule->getChar() == '.')
			{
				delete m_curSymbol;
				m_curSymbol = new Symbol(SymType::twopoints);
				m_ioModule->nextChar();
			}
			else
			{
				delete m_curSymbol;
				m_curSymbol = new Symbol(SymType::point);
			}
			break;
		case ',':
			delete m_curSymbol;
			m_curSymbol = new Symbol(SymType::comma);
			m_ioModule->nextChar();
			break;
		case '^':
			delete m_curSymbol;
			m_curSymbol = new Symbol(SymType::arrow);
			m_ioModule->nextChar();
			break;
		case ';':
			delete m_curSymbol;
			m_curSymbol = new Symbol(SymType::semicolon);
			m_ioModule->nextChar();
			break;
		default:
			m_ioModule->addError(6, m_symbolPos);
			m_ioModule->nextChar();
			break;
	}
}

void Lexer::scanIdentOrKeyword()
{
	int nameLength = 0;
	string name;
	int startLine = m_ioModule->getCharPosition()->getLineNumber();
	while ((isEnglishLetter(m_ioModule->getChar()) ||
		isdigit(m_ioModule->getChar())) && 
		nameLength < MAX_NAME_LENGTH)
	{
		int curLine = m_ioModule->getCharPosition()->getLineNumber();
		if (curLine != startLine)
			break;
		name += m_ioModule->getChar();
		nameLength++;
		m_ioModule->nextChar();
	}
	auto keyWordValue = m_keyWords.find(name);
	if (keyWordValue == m_keyWords.end())		
	{
		delete m_curSymbol;
		m_curSymbol = new Symbol(SymType::ident);
		m_curSymbol->setName(name);
		m_nameTable.insert(name);
	}
	else
	{
		delete m_curSymbol;
		m_curSymbol = new Symbol(keyWordValue->second);
	}
}

void Lexer::scanNumericConst()
{
	float number = 0;
	bool isErrorInt = false;
	while (isdigit(m_ioModule->getChar()))
	{
		int digit = m_ioModule->getChar() - '0';
		if (!(number < MAX_INT_VALUE / 10 ||
			(number == MAX_INT_VALUE / 10 &&	
				digit <= MAX_INT_VALUE % 10)))
		{
			isErrorInt = true;
		}
		else number = 10 * number + digit;
		m_ioModule->nextChar();
	}
	if (m_ioModule->getChar() != '.' && m_ioModule->getChar() != 'e')
	{
		if (isErrorInt)
			m_ioModule->addError(203, m_symbolPos);
		delete m_curSymbol;
		m_curSymbol = new Symbol(SymType::intc);
		m_curSymbol->setValue(!isErrorInt ? (int)number : MAX_INT_VALUE + 1);
	}
	else scanFloatConst(number);
}

void Lexer::scanFloatConst(float number)
{
	if (m_ioModule->getChar() == '.') 
		m_ioModule->nextChar();
	float deg = 10;
	while (isdigit(m_ioModule->getChar()))
	{
		number += (m_ioModule->getChar() - '0') / deg;
		deg *= 10;
		m_ioModule->nextChar();
	}
	if (m_ioModule->getChar() != 'e')
	{
		delete m_curSymbol;
		m_curSymbol = new Symbol(SymType::floatc);
		m_curSymbol->setValue(number);
		return;
	}
	m_ioModule->nextChar();
	int sign = 1;
	if (m_ioModule->getChar() == '+')
	{
		sign = 1;
		m_ioModule->nextChar();
	}
	else if (m_ioModule->getChar() == '-')
	{
		sign = -1;
		m_ioModule->nextChar();
	}
	if (!isdigit(m_ioModule->getChar()))
	{
		TextPosition pos(m_symbolPos.getLineNumber(),
			m_ioModule->getCharPosition()->getCharNumber());
		m_ioModule->addError(201, pos);
		m_ioModule->nextChar();
		return;
	}
	float order = 0;
	while (isdigit(m_ioModule->getChar()))
	{
		int digit = m_ioModule->getChar() - '0';
		order = 10 * order + digit;
		m_ioModule->nextChar();
	}
	m_curSymbol = new Symbol(SymType::floatc);
	m_curSymbol->setValue(number * (float)pow(10, order * sign));
}

void Lexer::scanCharConst()
{
	int startLine = m_ioModule->getCharPosition()->getLineNumber();
	m_ioModule->nextChar();
	string value = "";
	bool isError = false;
	while ((m_ioModule->getChar() != '\'' &&
			m_ioModule->getChar() != '\0'))
	{
		int curLine = m_ioModule->getCharPosition()->getLineNumber();
		if (!isError && curLine != startLine)
		{
			isError = true;
		}
		else value += m_ioModule->getChar();
		m_ioModule->nextChar();
	}
	char endChar = m_ioModule->getChar();
	if (isError || endChar == '\0' || value == "")
	{
		TextPosition lastPos(m_ioModule->getCharPosition()->getLineNumber(), 
			m_ioModule->getCharPosition()->getCharNumber());
		m_ioModule->addError(75, lastPos);
		m_ioModule->nextChar();
		return;
	}
	m_ioModule->nextChar();
	delete m_curSymbol;
	m_curSymbol = new Symbol(SymType::charc);
	m_curSymbol->setValue(value);
}

void Lexer::scanOneLineComment()
{
	int startLine = m_symbolPos.getLineNumber();
	m_ioModule->nextChar();
	while (m_ioModule->getCharPosition()->getLineNumber() == startLine)
		m_ioModule->nextChar();
	nextSymbol();
}

void Lexer::scanMultilineComment()
{
	bool isEndComment = false;
	int lastCharPos = m_ioModule->getCharPosition()->getCharNumber();
	while (!isEndComment)
	{
		m_ioModule->nextChar();
		if (m_ioModule->getChar() == '\0')
			break;
		if (m_ioModule->getChar() == '}')
		{
			isEndComment = true;
			break;
		}
		if (m_ioModule->getChar() == '*')
		{
			m_ioModule->nextChar();
			if (m_ioModule->getChar() == ')')
			{
				isEndComment = true;
			}
		}
		lastCharPos = m_ioModule->getCharPosition()->getCharNumber();
	}
	if (!isEndComment)
		m_ioModule->addError(86, TextPosition(m_ioModule->getCharPosition()->getLineNumber(), lastCharPos));
	m_ioModule->nextChar();
	nextSymbol();
}

bool Lexer::isEnglishLetter(char ch)
{
	return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
}


