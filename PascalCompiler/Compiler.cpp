#include "stdafx.h"
#include "Compiler.h"


Compiler::Compiler(string fileName, bool isFolder)
{
	setlocale(LC_ALL, "RUS");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	m_fileName = fileName;
	m_isFolder = isFolder;
}

void Compiler::Compile()
{
	try
	{
		if (m_isFolder)
		{
			cout << "������� ������� �����: " << m_fileName << endl;
			if (!fs::exists(m_fileName))
			{
				cout << "����� � ������� �� ����������. �������, ���-�� ����� �� ���?" << endl;
				return;
			}
			int filesCount = 0;
			for (auto & fileInDir : fs::directory_iterator(m_fileName))
			{
				m_lexer = new Lexer(fileInDir.path());
				m_synAnalizer = new SynSemAnalizer(m_lexer);
				m_synAnalizer->start();
				m_lexer->printProgram(true);
				delete m_lexer;
				m_lexer = nullptr;
				delete m_synAnalizer;
				filesCount++;
			}
			cout << "����� ������: " << filesCount << endl;
		}
		else
		{
			m_lexer = new Lexer(m_fileName);
			m_synAnalizer = new SynSemAnalizer(m_lexer);
			m_synAnalizer->start();
			m_lexer->printProgram(false);
			delete m_lexer;
			delete m_synAnalizer;
		}
	}
	catch (exception)
	{
		cout << "���-�� ����� �� ���. ���������� �� �������� �������� � ��������� ���������� ��� ���." << endl;
	}
}
