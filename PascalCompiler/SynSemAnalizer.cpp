#include "stdafx.h"
#include "SynSemAnalizer.h"

SynSemAnalizer::SynSemAnalizer(Lexer * lexer)
{
	m_lexer = lexer;
	m_isOpenedScopeInProc = false;
}

void SynSemAnalizer::start()
{
	m_lexer->nextSymbol();
	execAnalizeFunc(3, false,
		{ typesy, varsy, proceduresy, beginsy, programsy },
		{ SymType::point, endsy },
		[=]() {	analizeProgram(); });
}

TypeInformation * SynSemAnalizer::execAnalizeFunc(int error, bool isCanBeEmpty, unordered_set<SymType> starters, unordered_set<SymType> followers, function<TypeInformation*(void)> func)
{
	if (!isBelong(m_lexer->getCurrentSymbol()->getType(), starters) && !isCanBeEmpty && !m_lexer->isEnd)
	{
		m_lexer->addError(error);
		skipTo(starters, followers);
	}
	if (isBelong(m_lexer->getCurrentSymbol()->getType(), starters) && !m_lexer->isEnd)
	{
		TypeInformation* type = func();
		if (!isBelong(m_lexer->getCurrentSymbol()->getType(), followers))
		{
			m_lexer->addError(6);
			skipTo(followers);
		}
		return type;
	}
	return nullptr;
}

void SynSemAnalizer::execAnalizeFunc(int error, bool isCanBeEmpty, unordered_set<SymType> starters, unordered_set<SymType> followers, function<void(void)> func)
{
	if (!isBelong(m_lexer->getCurrentSymbol()->getType(), starters) && !isCanBeEmpty && !m_lexer->isEnd)
	{
		m_lexer->addError(error);
		skipTo(starters, followers);
	}
	if (isBelong(m_lexer->getCurrentSymbol()->getType(), starters) && !m_lexer->isEnd)
	{
		func();
		if (!isBelong(m_lexer->getCurrentSymbol()->getType(), followers) && !m_lexer->isEnd && !m_lexer->isEnd)
		{
			m_lexer->addError(6);
			skipTo(followers);
		}
	}
}

bool SynSemAnalizer::isBelong(SymType symbol, unordered_set<SymType> symbolSet)
{
	return symbolSet.find(symbol) != symbolSet.end();
}

void SynSemAnalizer::skipTo(unordered_set<SymType> followers)
{
	while (!isBelong(m_lexer->getCurrentSymbol()->getType(), followers))
		m_lexer->nextSymbol();
}

void SynSemAnalizer::skipTo(unordered_set<SymType> starters, unordered_set<SymType> followers)
{
	while (!isBelong(m_lexer->getCurrentSymbol()->getType(), followers) 
		   && !isBelong(m_lexer->getCurrentSymbol()->getType(), starters))
		m_lexer->nextSymbol();
}

string SynSemAnalizer::getCurSymbolName()
{
	return m_lexer->getCurrentSymbol()->getName();
}

void SynSemAnalizer::accept(SymType symbol)
{
	if (m_lexer->isEnd) return;
	if (m_lexer->getCurrentSymbol()->getType() == symbol)
		m_lexer->nextSymbol();
	else m_lexer->addError(symbol);
}

// <���������>::=program <���>;<����>.
void SynSemAnalizer::analizeProgram()
{
	openScope();
	initializeDummyScope();
	openScope();

	accept(programsy);
	if (m_lexer->getCurrentSymbol()->getType() == ident)
		addIdentifier(new Identifier(getCurSymbolName(), new ProgsIdentifier()));
	m_lexer->setProgramName(getCurSymbolName());
	accept(ident);
	accept(semicolon);
	execAnalizeFunc(18, false,
		{ typesy, varsy, proceduresy, beginsy }, 
		{ SymType::point, endsy }, 
		[=]() {analizeBlock(); });
	accept(point);
	while (!m_lexer->isEnd)
		m_lexer->nextSymbol();

	while (m_localScope != nullptr)
		closeScope();
}

// <����>::=<������ �����><������ ����������><������ ��������><������ ����������>
void SynSemAnalizer::analizeBlock()
{
	execAnalizeFunc(18, true,
		{ typesy },
		{ varsy, proceduresy, beginsy, SymType::point },
		[=]() {	analizeTypePart(); });
	execAnalizeFunc(18, true,
		{ varsy },
		{ proceduresy, beginsy, SymType::point },
		[=]() {	analizeVarPart(); });
	execAnalizeFunc(18, true,
		{ proceduresy },
		{ beginsy, SymType::point },
		[=]() {	analizeProcPart(); });
	execAnalizeFunc(17, false,
		{ beginsy },
		{ SymType::point, endsy, beginsy, semicolon, proceduresy },
		[=]() {	analizeCompositeStatement(); }); // <������ ����������>:=<��������� ��������>			
}

// <������ �����>::=<�����>|type <����������� ����>; {<����������� ����>;}
void SynSemAnalizer::analizeTypePart()
{
	if (m_lexer->getCurrentSymbol()->getType() == typesy)
	{
		accept(typesy);
		do
		{
			analizeTypeDefinition();
			accept(semicolon);
		} while (m_lexer->getCurrentSymbol()->getType() == ident);
	}
}

//<������ ����������>::= var <�������� ���������� ����������>;
// {<�������� ���������� ����������>;}|<�����>
void SynSemAnalizer::analizeVarPart()
{
	if (m_lexer->getCurrentSymbol()->getType() == varsy)
	{
		accept(varsy);
		do
		{
			analizeSameVarDeclaration();
			accept(semicolon);
		} while (m_lexer->getCurrentSymbol()->getType() == ident);
	}
}

// <�������� ���������� ����������>::=<���>{,<���>}:<���>
void SynSemAnalizer::analizeSameVarDeclaration()
{
	vector<Identifier*> sameVariables;
	newVariable(sameVariables);
	accept(ident);
	while (m_lexer->getCurrentSymbol()->getType() == comma)
	{
		m_lexer->nextSymbol();
		newVariable(sameVariables);
		accept(ident);
	}
	accept(colon);
	TypeInformation* typeInfo = analizeType();
	for (auto &variable : sameVariables)
		variable->setTypeInfo(typeInfo);
}

// <�������� ���������>::={<��������� ���������><����>}
void SynSemAnalizer::analizeProcPart()
{
	while (m_lexer->getCurrentSymbol()->getType() == proceduresy)
	{
		execAnalizeFunc(6, false,
			{ proceduresy },
			{ typesy, varsy, proceduresy, beginsy },
			[=]() {	analizeProcHeader(); });
		execAnalizeFunc(6, false,
			{ typesy, varsy, proceduresy, beginsy },
			{ proceduresy, semicolon, point, beginsy },
			[=]() {	analizeBlock(); });
		if (m_isOpenedScopeInProc)
		{
			m_localScope = m_localScope->getEnclosingScope();		// scope ����������� � analizeProcHeader
			m_isOpenedScopeInProc = false;
		}
	}
}

// <��������� ���������>::= procedure <���>;
//| procedure <���>(<������ ���������� ����������>{;<������ ���������� ����������>});
void SynSemAnalizer::analizeProcHeader()
{
	accept(proceduresy);
	Identifier* procIdent = nullptr;
	if (m_lexer->getCurrentSymbol()->getType() == ident)
	{
		procIdent = new Identifier(getCurSymbolName(), new ProcsIdentifier());
		addIdentifier(procIdent);
	}
	accept(ident);
	openScope();
	m_isOpenedScopeInProc = true;
	if (m_lexer->getCurrentSymbol()->getType() == semicolon)
		accept(semicolon);
	else
	{
		if (m_lexer->getCurrentSymbol()->getType() != leftpar)
			skipTo({ leftpar, rightpar, ident, point, semicolon });
		if (m_lexer->getCurrentSymbol()->getType() != leftpar)
		{
			accept(leftpar);
			return;
		}
		accept(leftpar);
		analizeFormalParamsPart(procIdent);
		while (m_lexer->getCurrentSymbol()->getType() == semicolon)
		{
			accept(semicolon);
			analizeFormalParamsPart(procIdent);
		}
		accept(rightpar);
		accept(semicolon);
	}
}

// <������ ���������� ����������>::=<������ ����������>
// | var <������ ����������>
// |procedure <���>{,<���>}
void SynSemAnalizer::analizeFormalParamsPart(Identifier* procIdent)
{
	switch (m_lexer->getCurrentSymbol()->getType())
	{
		case ident:
			analizeParamsGroup(false, procIdent);
			break;
		case varsy:
			accept(varsy);
			analizeParamsGroup(true, procIdent);
			break;
	}
}

// <������ ����������>::=<���>{,<���>}:<��� ����>
void SynSemAnalizer::analizeParamsGroup(bool isByReference, Identifier* procedureIdent)
{
	vector<Identifier*> idents;
	if (m_lexer->getCurrentSymbol()->getType() == ident)
		idents.push_back(new Identifier(getCurSymbolName(), new ProcParam(isByReference)));
	accept(ident);
	while (m_lexer->getCurrentSymbol()->getType() == comma)
	{
		accept(comma);
		if (m_lexer->getCurrentSymbol()->getType() == ident)
			idents.push_back(new Identifier(getCurSymbolName(), new ProcParam(isByReference)));
		accept(ident);
	}
	accept(colon);
	Identifier* typeIdent = m_localScope->findIndentifier(getCurSymbolName());
	TypeInformation* typeInfo = nullptr;
	if (typeIdent != nullptr)
		typeInfo = typeIdent->getTypeInfo();
	ProcsIdentifier* mainProc = nullptr;
	if (procedureIdent != nullptr)
		mainProc = static_cast<ProcsIdentifier*>(procedureIdent->getUseMethod());
	for (auto & procParam : idents)
	{
		addIdentifier(procParam);
		if (mainProc != nullptr)
			mainProc->addProcParam(procParam);
		if (typeInfo != nullptr) 
			procParam->setTypeInfo(typeInfo);
	}
	accept(ident);
}

// <��������� ��������>::= begin<��������>{;<��������>} end
void SynSemAnalizer::analizeCompositeStatement()
{
	accept(beginsy);
	execAnalizeFunc(6, true,
		{ beginsy, ifsy, whilesy, repeatsy, forsy, ident },
		{ endsy, semicolon, SymType::point },
		[=]() {	analizeStatement(); });
	while (m_lexer->getCurrentSymbol()->getType() == semicolon)
	{
		accept(semicolon);
		execAnalizeFunc(6, true,
			{ beginsy, ifsy, whilesy, repeatsy, forsy, ident },	
			{ endsy, semicolon, SymType::point },
			[=]() {	analizeStatement(); });
	}
	accept(endsy);
}

// <��������>::=<������� ��������>|<������� ��������>
void SynSemAnalizer::analizeStatement()
{
	switch (m_lexer->getCurrentSymbol()->getType())
	{
		case beginsy:
			analizeCompositeStatement();
			break;
		case ifsy:
			analizeIfStatement();
			break;
		case whilesy:
			analizeWhileStatement();
			break;
		case repeatsy:
			analizeRepeatStatement();
			break;
		case forsy:
			analizeForStatement();
			break;
		case ident:
			Identifier * identifier = m_localScope->findIndentifier(getCurSymbolName());
			if (identifier != nullptr)
			{
				if (identifier->getUseMethod()->getUseCode() == UseMethod::Procs)
					analizeProcedureOperator();
				else analizeAssignOperator();
			}
			// ������������� ������������� ������?
			else
			{
				m_lexer->addError(104);
				addIdentifier(new Identifier(getCurSymbolName(), new VarsIdentifier()));
				analizeAssignOperator();
			}
			break;
	}
}

// <�������� ������������>::=<����������> := <���������>
void SynSemAnalizer::analizeAssignOperator()
{
	Identifier* leftIdent = m_localScope->findIndentifier(getCurSymbolName());
	if (leftIdent->getUseMethod()->getUseCode() != UseMethod::Vars &&
		leftIdent->getUseMethod()->getUseCode() != UseMethod::ProcsParam)
		m_lexer->addError(100);
	TypeInformation* leftType = nullptr;
	if (leftIdent != nullptr && leftIdent->getUseMethod()->getUseCode() == UseMethod::Vars)
		leftType = leftIdent->getTypeInfo();
	// ������������� ������������� ������?
	else
	{
		if (leftIdent == nullptr) m_lexer->addError(104);
		addIdentifier(new Identifier(getCurSymbolName(), new VarsIdentifier()));
	}
	accept(ident);
	accept(assign);
	TextPosition curPos = m_lexer->getCurrentPosition();
	TypeInformation* rightType = analizeExpression();
	if (rightType != nullptr && leftType != nullptr)
	{
		if (!leftType->isCompatible(rightType)
			&& !rightType->isCompatible(leftType))
			m_lexer->addError(145, curPos);
	}
	else if (rightType == nullptr 
			 && m_lexer->getCurrentSymbol()->getType() == ident) 
		m_lexer->addError(104);
}

// <���� � ����������>::= 
// for <�������� �����>:=<���������><�����������><���������> do <��������>
void SynSemAnalizer::analizeForStatement()
{
	accept(forsy);

	Identifier* varIdent = m_localScope->findIndentifier(getCurSymbolName());
	if (varIdent != nullptr 
		&& varIdent->getTypeInfo() != nullptr)
	{
		auto typeCode = varIdent->getTypeInfo()->getTypeCode();
		if (typeCode != TypeInformation::Scalars
			&& typeCode != TypeInformation::Enums
			|| varIdent->getTypeInfo() == m_realType)
		{
			m_lexer->addError(143);
		}
	}

	accept(ident);
	accept(assign);

	TypeInformation* expType = analizeExpression();
	if (!varIdent->getTypeInfo()->isCompatible(expType))
		m_lexer->addError(145);
	if (m_lexer->getCurrentSymbol()->getType() == tosy ||
		m_lexer->getCurrentSymbol()->getType() == downtosy)
		m_lexer->nextSymbol();
	else m_lexer->addError(55);
	expType = analizeExpression();
	if (!varIdent->getTypeInfo()->isCompatible(expType))
		m_lexer->addError(145);
	accept(dosy);
	analizeStatement();
}

//<���� � ������������>::= while <���������> do <��������>
void SynSemAnalizer::analizeWhileStatement()
{
	accept(whilesy);
	TypeInformation* typeExp = analizeExpression();
	if (!typeExp->isCompatible(m_booleanType))
		m_lexer->addError(135);
	accept(dosy);
	analizeStatement();
}

// <���� � ������������>::= repeat <��������>{;<��������>} until <���������>
void SynSemAnalizer::analizeRepeatStatement()
{
	accept(repeatsy);
	analizeStatement();
	while (m_lexer->getCurrentSymbol()->getType() == semicolon)
	{
		accept(semicolon);
		analizeStatement();
	}
	accept(untilsy);
	TypeInformation* typeExp = analizeExpression();
	if (typeExp != nullptr && !typeExp->isCompatible(m_booleanType))
		m_lexer->addError(135);
}

// <�������� ��������>::= if <���������> then <��������>
// | if <���������> then <��������> else <��������>
void SynSemAnalizer::analizeIfStatement()
{
	accept(ifsy);
	function<TypeInformation*(void)> analizeExpressionFunc = [this]() {return analizeExpression(); };
	TextPosition curPos = m_lexer->getCurrentPosition();
	TypeInformation* typeInfo = execAnalizeFunc(6, false,
		{ ident, SymType::plus, SymType::minus, SymType::intc, SymType::floatc, SymType::charc, leftpar },
		{ thensy, beginsy, endsy, semicolon, point },
		analizeExpressionFunc);
	if (!typeInfo->isCompatible(m_booleanType))
		m_lexer->addError(135, curPos);
	accept(thensy);
	execAnalizeFunc(6, true,
		{ beginsy, ifsy, whilesy, repeatsy, forsy, ident },
		{ endsy, semicolon, SymType::point, elsesy },
		[=]() {	analizeStatement(); });
	if (m_lexer->getCurrentSymbol()->getType() == elsesy)
	{ 
		accept(elsesy);
		execAnalizeFunc(6, true,
			{ beginsy, ifsy, whilesy, repeatsy, forsy, ident },
			{ endsy, semicolon, SymType::point },
			[=]() {	analizeStatement(); });
	}
}

// <�������� ���������>::=<��� ���������>|<��� ���������>(<����������� ��������>{,<����������� ��������>})
void SynSemAnalizer::analizeProcedureOperator()
{
	ProcsIdentifier* procIdent = static_cast<ProcsIdentifier*>(m_localScope->findIndentifier(getCurSymbolName())->getUseMethod());
	bool isTooMany = false;
	accept(ident);
	if (procIdent->getParamsCount() > 0 && m_lexer->getCurrentSymbol()->getType() != leftpar)
	{
		isTooMany = true;
		m_lexer->addError(198);
	}
	if (m_lexer->getCurrentSymbol()->getType() == leftpar)
	{
		accept(leftpar);
		int i = -1;
		if (procIdent->getParamsCount() <= 0 && !isTooMany)
		{
			isTooMany = true;
			m_lexer->addError(198);
		}
		analizeActualParam(procIdent, ++i);
		while (m_lexer->getCurrentSymbol()->getType() == comma)
		{
			accept(comma);
			analizeActualParam(procIdent, ++i);
			if (i >= procIdent->getParamsCount() && !isTooMany)
			{
				isTooMany = true;
				m_lexer->addError(198);
			}
		}
		accept(rightpar);
	}
}

void SynSemAnalizer::analizeActualParam(ProcsIdentifier* procIdent, int paramNum)
{
	if (m_lexer->getCurrentSymbol()->getType() != ident)
	{
		Identifier* procParamIdent = procIdent->getParam(paramNum);
		TypeInformation* typeActual = analizeExpression();
		if (typeActual != nullptr && procParamIdent != nullptr && paramNum <= procIdent->getParamsCount())
		{
			TypeInformation* sType = procParamIdent->getTypeInfo();
			if (!typeActual->isCompatible(sType) && !sType->isCompatible(typeActual))
				m_lexer->addError(199);
		}
		return;
	}
	else if (paramNum <= procIdent->getParamsCount())
	{
		Identifier* procActualIdent = m_localScope->findIndentifier(getCurSymbolName());
		Identifier* procParamIdent = procIdent->getParam(paramNum);
		if (procParamIdent != nullptr &&
			procActualIdent != nullptr)
		{
			TypeInformation* fType = procActualIdent->getTypeInfo();
			TypeInformation* sType = procParamIdent->getTypeInfo();
			if (!sType->isCompatible(fType) && !fType->isCompatible(sType))
				m_lexer->addError(199);
		}
	}
	accept(ident);
	
}

// <���������>::=<������� ���������>
// |<������� ���������><�������� ���������><������� ���������>
TypeInformation* SynSemAnalizer::analizeExpression()
{
	TypeInformation* firstType = analizeSimpleExpression();
	if (isRelationOperation())
	{
		m_lexer->nextSymbol();
		TextPosition curPos = m_lexer->getCurrentPosition();
		TypeInformation* secondType = analizeSimpleExpression();
		if (!firstType->isCompatible(secondType)
			&& !secondType->isCompatible(firstType))
			m_lexer->addError(182, curPos);
		else firstType = m_booleanType;
	}
	return firstType;
}

// <������� ���������>:: = 
// <����><���������>{ <���������� ��������><���������> }
TypeInformation* SynSemAnalizer::analizeSimpleExpression()
{
	bool isContainSign = analizeSign();
	TypeInformation* firstType = analizeTerm();
	if (isContainSign)
		checkRightSign(firstType);
	while (isAdditiveOperation())
	{
		SymType symType = m_lexer->getCurrentSymbol()->getType();
		m_lexer->nextSymbol();
		TypeInformation* secondType = analizeTerm();
		checkCorrectOperation(firstType, secondType, symType);
	}
	return firstType;
}

void SynSemAnalizer::checkRightSign(TypeInformation* type)
{
	if (type == nullptr
		|| type->isCompatible(m_intType)
		|| type->isCompatible(m_realType))
		return;
	m_lexer->addError(211);
}

void SynSemAnalizer::checkLogical(TypeInformation* type)
{
	if (type != m_booleanType)
		m_lexer->addError(210);
}

void SynSemAnalizer::checkCorrectOperation(TypeInformation* firstType, TypeInformation* secondType, SymType symType)
{
	if (firstType == nullptr || secondType == nullptr)
		return;
	bool test = secondType->isCompatible(m_intType);
	switch (symType)
	{
		case SymType::orsy:
		case SymType::andsy:
			if (!(firstType->isCompatible(m_booleanType) &&
				secondType->isCompatible(m_booleanType)))
				m_lexer->addError(210);
			break;
		case SymType::plus:
		case SymType::minus:
			if (!((firstType->isCompatible(m_intType)
				|| firstType->isCompatible(m_realType))
				&& (secondType->isCompatible(m_intType)
				|| secondType->isCompatible(m_realType))))
				m_lexer->addError(211);
			break;
		case SymType::divsy:
		case SymType::modsy:
			if (!(firstType->isCompatible(m_intType)
				&& secondType->isCompatible(m_intType)))
				m_lexer->addError(212);
			break;
		case SymType::star:
			if (!((firstType->isCompatible(m_intType)
				|| firstType->isCompatible(m_realType))
				&& (secondType->isCompatible(m_intType)
				|| secondType->isCompatible(m_realType))))
				m_lexer->addError(213);
			break;
		case SymType::slash:
			if (!((firstType->isCompatible(m_intType)
				|| firstType->isCompatible(m_realType))
				&& (secondType->isCompatible(m_intType)
				|| secondType->isCompatible(m_realType))))
				m_lexer->addError(214);
			break;
	}
}

bool SynSemAnalizer::analizeSign()
{
	auto curSym = m_lexer->getCurrentSymbol()->getType();
	if (curSym == SymType::plus
		|| curSym == SymType::minus)
	{
		m_lexer->nextSymbol();
		return true;
	}
	return false;
	
}

//<���������>::=<���������>{<����������������� ��������><���������>}
TypeInformation* SynSemAnalizer::analizeTerm()
{
	TypeInformation* firstType = analizeFactor();
	while (isMultiplicativeOp())
	{
		SymType symType = m_lexer->getCurrentSymbol()->getType();
		m_lexer->nextSymbol();
		TypeInformation* secondType = analizeFactor();
		checkCorrectOperation(firstType, secondType, symType);
	}
	return firstType;
}

// <���������>::=<����������>|(<���������>)|not <���������>
TypeInformation* SynSemAnalizer::analizeFactor()
{
	TypeInformation* expType = nullptr;
	switch (m_lexer->getCurrentSymbol()->getType())
	{
		case leftpar:
			accept(leftpar);
			expType = analizeExpression();
			accept(rightpar);
			break;
		case notsy:
			accept(notsy);
			expType = analizeFactor();
			checkLogical(expType);
			break;
		// ��������� ��� �����
		case nilsy:
			accept(nilsy);
			expType = m_nilType;
			break;
		case floatc:
			accept(floatc);
			expType = m_realType;
			break;
		case intc:
			accept(intc);
			expType = m_intType;
			break;
		case charc:
			if (m_lexer->getCurrentSymbol()->getCharValue().size() > 1)
				expType = m_stringType;
			else expType = m_charType;
			accept(charc);
			break;
		case ident:
			Identifier * expIdent = m_localScope->findIndentifier(getCurSymbolName());
			if (expIdent != nullptr)
			{
				if (expIdent->getUseMethod()->getUseCode() == UseMethod::Consts
					|| expIdent->getUseMethod()->getUseCode() == UseMethod::Vars)
					expType = expIdent->getTypeInfo();
				else 
				{
					m_lexer->addError(100);
					expType = nullptr;
				}
			}
			else
			{
				m_lexer->addError(104);
				expType = nullptr;
			}
			accept(ident);
			break;
	}
	return expType;
}

bool SynSemAnalizer::isAdditiveOperation()
{
	auto curSym = m_lexer->getCurrentSymbol()->getType();
	return curSym == SymType::plus || curSym == SymType::minus ||
		curSym == orsy;
}

bool SynSemAnalizer::isRelationOperation()
{
	auto curSym = m_lexer->getCurrentSymbol()->getType();
	return curSym == SymType::equal || curSym == later ||
		curSym == SymType::greater || curSym == laterequal ||
		curSym == greaterequal || curSym == latergreater ||
		curSym == insy;
}

bool SynSemAnalizer::isMultiplicativeOp()
{
	auto curSym = m_lexer->getCurrentSymbol()->getType();
	return curSym == star || curSym == slash ||
		curSym == divsy || curSym == modsy || curSym == andsy;
}

// <���> ::= <���>
TypeInformation* SynSemAnalizer::analizeType()
{
	TypeInformation * resultType = nullptr;
	if (m_lexer->getCurrentSymbol()->getType() == ident)
	{
		Identifier* identType = m_localScope->findIndentifier(getCurSymbolName());
		if (identType != nullptr)
			resultType = identType->getTypeInfo();
		else m_lexer->addError(104);
	}
	accept(ident);
	return resultType;
}

// <����������� ����>::=<���>=<���>
void SynSemAnalizer::analizeTypeDefinition()
{
	Identifier* identifier = new Identifier(getCurSymbolName(), new TypesIdentifier());
	accept(ident);
	accept(SymType::equal);
	TypeInformation* type = new TypeInformation();
	type->setCompatibleType(analizeType());
	identifier->setTypeInfo(type);
	addIdentifier(identifier);
	m_localScope->addType(type);
}

void SynSemAnalizer::openScope()
{
	Scope* newScope = new Scope(m_localScope);
	m_localScope = newScope;
}

void SynSemAnalizer::closeScope()
{
	if (m_localScope == nullptr) return;
	Scope* oldScope = m_localScope;
	m_localScope = m_localScope->getEnclosingScope();
	delete oldScope;
}

void SynSemAnalizer::addIdentifier(Identifier * ident)
{
	m_lexer->addToNameTable(ident->getName());
	m_localScope->addIdentifier(ident);
}

void SynSemAnalizer::initializeDummyScope()
{
	/* ������������� ���� boolean */
	m_booleanType = new EnumType();
	m_booleanType->AddEnum("true");
	m_booleanType->AddEnum("false");

	addIdentifier(new Identifier("false", new ConstsIdentifier(), m_booleanType));
	addIdentifier(new Identifier("true", new ConstsIdentifier(), m_booleanType));
	addIdentifier(new Identifier("boolean", new TypesIdentifier(), m_booleanType));
	m_localScope->addType(m_booleanType);

	/* ������������� ��������� ����� */
	m_intType = new ScalarType();
	m_charType = new ScalarType();
	m_stringType = new ScalarType();
	m_realType = new ScalarType();

	addIdentifier(new Identifier("integer", new TypesIdentifier(), m_intType));
	addIdentifier(new Identifier("char", new TypesIdentifier(), m_charType));
	addIdentifier(new Identifier("real", new TypesIdentifier(), m_realType));
	addIdentifier(new Identifier("string", new TypesIdentifier(), m_stringType));

	m_localScope->addType(m_intType);
	m_localScope->addType(m_charType);
	m_localScope->addType(m_realType);
	
	m_intType->setCompatibleType(m_realType);
	m_charType->setCompatibleType(m_stringType);

	/* ������������� ��������� nil */
	m_nilType = new TypeInformation();
	addIdentifier(new Identifier("nil", new ConstsIdentifier(), m_nilType));
	m_localScope->addType(m_nilType);
}

void SynSemAnalizer::newVariable(vector<Identifier*>& sameVariables)
{
	Identifier* ident = new Identifier(getCurSymbolName(), new VarsIdentifier());
	addIdentifier(ident);
	sameVariables.push_back(ident);
}

