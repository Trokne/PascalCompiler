#pragma once
class TypeInformation
{
public:
	enum TypeCode {
		Scalars = 401,		// ����������� ��������� ���
		Limited = 402,		// ������������ ���
		Enums = 403,		// ������������ ���
		Arrays = 404,		// ���������� ��� (������)
		References = 405,	// ��������� ���
		Sets = 406,			// ������������� ���
		File = 407,			// �������� ���
		Records = 408		// ��������������� ��� (������)
	};
	TypeCode getTypeCode();
	TypeInformation* getComatibleType();
	void setCompatibleType(TypeInformation* type);
	bool isCompatible(TypeInformation* secondType);
private:
	TypeInformation * m_compatibleType;
protected:
	TypeCode m_typeCode;
};

