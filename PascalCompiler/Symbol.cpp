#include "stdafx.h"
#include "Symbol.h"

Symbol::Symbol(SymType symType)
{
	m_type = symType;
}

SymType Symbol::getType()
{
	return m_type;
}

string Symbol::getName()
{
	return m_name;
}

void Symbol::setType(SymType symType)
{
	m_type = symType;
}

void Symbol::setValue(int value)
{
	m_nmbInt = value;
	m_nmbFloat = numeric_limits<float>::max();
	m_constStr = "";
	m_name = "";
}

void Symbol::setValue(float value)
{
	m_nmbFloat = value;
	m_nmbInt = numeric_limits<int>::max();
	m_constStr = "";
	m_name = "";
}

void Symbol::setValue(string value)
{
	m_nmbInt = numeric_limits<int>::max();
	m_nmbFloat = numeric_limits<float>::max();
	m_constStr = value;
	m_name = "";
}

void Symbol::setName(string name)
{
	m_nmbInt = numeric_limits<int>::max();
	m_nmbFloat = numeric_limits<float>::max();
	m_constStr = "";
	m_name = name;
}

string Symbol::getCharValue()
{
	return m_constStr;
}
