#include "stdafx.h"
#include "EnumType.h"


EnumType::EnumType()
{
	m_typeCode = TypeCode::Enums;
}

EnumType::EnumType(vector<string> names)
{
	m_typeCode = TypeCode::Enums;
	m_names = names;
}

EnumType::EnumType(string firstName)
{
	m_typeCode = TypeCode::Enums;
	m_names.push_back(firstName);
}

EnumType::~EnumType()
{
	m_typeCode = TypeCode::Enums;
}

void EnumType::AddEnum(string name)
{
	m_names.push_back(name);
}

vector<string> EnumType::getEnums()
{
	return vector<string>();
}
