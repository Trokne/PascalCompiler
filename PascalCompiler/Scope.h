#pragma once
#include <unordered_set>
#include "Identifier.h"
#include "TypeInformation.h"

using namespace std;

class Scope
{
public:
	Scope(Scope*);
	Scope();
	~Scope();

	bool addIdentifier(Identifier*);
	bool addType(TypeInformation*);
	Scope* getEnclosingScope();
	Identifier* findIndentifier(string);
	
private:
	unordered_set<Identifier*> m_tableIdentifiers;
	unordered_set<TypeInformation*> m_tableTypes;
	Scope* m_enclosingScope;
	vector<TypeInformation*> m_typeInfo;
};

