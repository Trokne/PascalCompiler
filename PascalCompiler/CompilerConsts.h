#pragma once
#include <string>


static const int MAX_INT_VALUE = 32767;
static const int MAX_NAME_LENGTH = 256;
static const int MAX_ERRORS_COUNT = 4;
static const std::string DEFAULT_PATH = "E:\\tests\\result\\";