#pragma once
#include "TextPosition.h"

class Error
{
public:
	Error(int code, TextPosition position);
	TextPosition getPosition();
	int getCode();

private:
	int m_code;
	TextPosition m_position;
};

