#include "stdafx.h"
#include "TypeInformation.h"

TypeInformation::TypeCode TypeInformation::getTypeCode()
{
	return m_typeCode;
}

TypeInformation * TypeInformation::getComatibleType()
{
	return m_compatibleType;
}

void TypeInformation::setCompatibleType(TypeInformation * type)
{
	m_compatibleType = type;
}

bool TypeInformation::isCompatible(TypeInformation * secondType)
{
	if (secondType == nullptr || this == nullptr || this == secondType)
		return true;
	else if (this != nullptr && secondType->getComatibleType() != nullptr)
		return isCompatible(secondType->getComatibleType());
	else if (secondType != nullptr && this != nullptr && getComatibleType() != nullptr)
		return getComatibleType()->isCompatible(secondType);
	else return false;

	//if (this == secondType
	//	|| (m_compatibleType != nullptr && m_compatibleType == secondType))
	//	return true;
	//else if (m_compatibleType != nullptr)
	//	return m_compatibleType->isCompatible(secondType);
	//else if (secondType->getComatibleType() != nullptr && this != nullptr)
	//	return isCompatible(secondType->getComatibleType());
	//else return false;
}

