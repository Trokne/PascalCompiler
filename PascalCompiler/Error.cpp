#include "stdafx.h"
#include "Error.h"

Error::Error(int code, TextPosition position)
{
	m_code = code;
	m_position = TextPosition(position.getLineNumber(), position.getCharNumber());
}

TextPosition Error::getPosition()
{
	return m_position;
}

int Error::getCode()
{
	return m_code;
}
