#pragma once
#include "UseMethod.h"
#include "Identifier.h"
#include "ProcParam.h"
#include <vector>

class ProcsIdentifier :
	public UseMethod
{
public:
	ProcsIdentifier();
	void addProcParam(Identifier* param);
	Identifier* getParam(int index);
	size_t getParamsCount();
private:
	vector<Identifier*> m_params;
	bool m_isForwad;
};

