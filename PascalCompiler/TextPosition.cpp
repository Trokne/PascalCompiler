#include "stdafx.h"
#include "TextPosition.h"


TextPosition::TextPosition(unsigned lineNumber, unsigned charNumber)
{
	m_lineNumber = lineNumber;
	m_charNumber = charNumber;
}

TextPosition::TextPosition()
{
	m_lineNumber = 0;
	m_charNumber = -1;
}


unsigned TextPosition::getLineNumber()
{
	return m_lineNumber;
}

unsigned TextPosition::getCharNumber()
{
	return m_charNumber;
}

void TextPosition::setLineNumber(unsigned lineNumber)
{
	m_lineNumber = lineNumber;
}

void TextPosition::setCharNumber(unsigned charNumber)
{
	m_charNumber = charNumber;
}
