#pragma once

enum SymType
{
	star = 21,				// *
	slash = 60,				// /
	equal = 16,				// =
	comma = 20,				// ,
	semicolon = 14,			// ;
	colon = 5,				// :
	point = 61,				// .
	arrow = 62,				// ^
	leftpar = 9,			// (
	rightpar = 4,			// )
	lbracket = 11,			// [
	rbracket = 12,			// ]
	flpar = 63,				// {
	frpar = 64,				// }
	later = 65,				// <
	greater = 66,			// >
	laterequal = 67,		// <=
	greaterequal = 68,		// >=
	latergreater = 69,		// <>
	plus = 70,				// +
	minus = 71,				// -
	lcomment = 72,			// (*
	rcomment = 73,			// *)
	assign = 51,			// :=
	twopoints = 74,			// ..
	ident = 2,				// �������������
	floatc = 82,			// �������. ���������
	intc = 15,				// ����� ���������
	charc = 83,				// ����. ���������
	emptyFile = -1,
	/* * * * * �������� ����� * * * */
	/* ������ 2 */
	ifsy = 56,
	dosy = 54,
	ofsy = 8,
	orsy = 1317,
	insy = 1318,
	tosy = 55,
	/* ������ 3 */
	endsy = 13,
	varsy = 1309,
	divsy = 1310,
	andsy = 1311,
	notsy = 1312,
	forsy = 1313,
	modsy = 1314,
	nilsy = 1315,
	setsy = 1316,
	/* ������ 4 */
	thensy = 52,
	elsesy = 32,
	casesy = 31,
	filesy = 57,
	gotosy = 33,
	typesy = 34,
	withsy = 37,
	/* ������ 5 */
	beginsy = 17,
	whilesy = 1319,
	arraysy = 1320,
	constsy = 1321,
	labelsy = 1322,
	untilsy = 53,
	/* ������ 6 */
	downtosy = 58,
	packedsy = 1323,
	recordsy = 1324,
	repeatsy = 1325,
	/* ������ 7 */
	programsy = 3,
	/* ������ 8 */
	functionsy = 1326,
	/* ������ 9 */
	proceduresy = 1327
};