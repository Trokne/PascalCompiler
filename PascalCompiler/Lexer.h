#pragma once
#include "InputOutput.h"
#include "Symbol.h"

#include <cmath>
#include <unordered_set>

using namespace std;

class Lexer
{
public:
	Lexer(fs::path filePath);
	~Lexer();
	void printProgram(bool isDir);
	void nextSymbol();
	void addError(int errorCode, TextPosition position);
	void addError(int errorCode);
	Symbol* getCurrentSymbol();
	TextPosition getCurrentPosition();
	bool isEnd;
	bool addToNameTable(string name);
	bool isContainInNameTable(string name);
	void setProgramName(string name);
private:
	TextPosition m_symbolPos;
	Symbol* m_curSymbol;
	map<string, SymType> m_keyWords;
	unordered_set<string> m_nameTable;
	InputOutput* m_ioModule;

	void generateKeyWordsMap();
	void scanSymbol();
	void scanIdentOrKeyword();
	void scanNumericConst();
	void scanFloatConst(float number);
	void scanCharConst();
	void scanOneLineComment();
	void scanMultilineComment();
	bool isEnglishLetter(char ch);
};

