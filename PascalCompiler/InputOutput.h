#pragma once
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <Windows.h>
#include <stdexcept>
#include <filesystem>

#include "Error.h"
#include "TextPosition.h"
#include "CompilerConsts.h"

using namespace std;
namespace fs = std::experimental::filesystem;

class InputOutput
{
public:
	InputOutput(fs::path);
	~InputOutput();
	void printProgram(bool);
	void nextChar();
	char getChar();
	TextPosition* getCharPosition();
	void addError(int, TextPosition);
	void setProgramName(string);

private:
	string m_programName;
	vector<string> m_programText;
	char m_currentChar;
	TextPosition* m_textPosition;
	size_t m_currentLineLength;
	map<int, string> m_errorCodes;
	vector<Error*> m_errors;
	int m_currentErrorIndex;
	int m_errorNumLength;
	fs::path m_filePath;
	
	void showErrors(int, bool, ofstream &);
	void readAllFile(string);
	void generateErrorCodes();
	void showCurrentLine();
	int digitCount(int);
	int getPrintOffset();
};

