#include "stdafx.h"
#include "Scope.h"


Scope::Scope(Scope * ownedScope)
{
	m_enclosingScope = ownedScope;
}

Scope::Scope()
{
	m_enclosingScope = nullptr;
}

Scope::~Scope()
{
	for (auto &ident : m_tableIdentifiers)
		delete ident;
	for (auto &type : m_tableTypes)
		delete type;
}

bool Scope::addIdentifier(Identifier * identifier)
{
	return (m_tableIdentifiers.insert(identifier)).second;
}

bool Scope::addType(TypeInformation * type)
{
	return (m_tableTypes.insert(type)).second;
}

Scope * Scope::getEnclosingScope()
{
	return m_enclosingScope;
}

Identifier * Scope::findIndentifier(string name)
{
	for (auto& ident : m_tableIdentifiers)
	{
		if (ident->getName() == name)
			return ident;
	}
	if (m_enclosingScope != nullptr)
		return m_enclosingScope->findIndentifier(name);
	else return nullptr;
}
