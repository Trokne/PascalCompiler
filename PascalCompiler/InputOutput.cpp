#include "stdafx.h"
#include "InputOutput.h"


InputOutput::InputOutput(fs::path filePath)
{
	m_filePath = filePath;
	readAllFile(filePath.string());
	generateErrorCodes();
	m_textPosition = new TextPosition(0, -1);
	m_currentLineLength = m_programText[0].length();
	m_currentChar = ' ';
	m_currentErrorIndex = 0;
	m_errorNumLength = digitCount(MAX_ERRORS_COUNT) + digitCount((int)m_programText.size() + 1);
}

InputOutput::~InputOutput()
{
	delete m_textPosition;
	for (auto & error : m_errors)
		delete error;
}

void InputOutput::printProgram(bool isFolder)
{
	ofstream outStream;
	if (isFolder)
	{
		if (!fs::exists(DEFAULT_PATH))
		{
			if (!fs::create_directory(DEFAULT_PATH))
			{
				cout << "���������� ������� ����� � ������������ ������. ��������� ������������ ����.";
				return;
			}
		}
		outStream.open(DEFAULT_PATH + m_filePath.filename().string());
	}
	if (isFolder) 
		cout << "����� ����� " << m_filePath.filename().filename().string() << " ������� �������!" << endl;
	else cout << "����� ��������� " << m_programName << ":" << endl;
	for (int i = 0; i < m_programText.size(); i++)
	{
		if (isFolder)
			outStream << i + 1 << ".";
		else cout << i + 1 << ".";
		for (int i = 0; i < getPrintOffset(); i++)
		{
			if (isFolder)
				outStream << " ";
			else cout << " ";
		}
		if (isFolder)
			outStream << m_programText[i] << endl;
		else cout << m_programText[i] << endl;
		showErrors(i, isFolder, outStream);
	}
	if (isFolder) 
		outStream.close();
}

void InputOutput::showErrors(int line, bool isFolder, ofstream &outStream)
{
	for (auto error : m_errors)
	{
		if (error->getPosition().getLineNumber() != line)
			continue;
		m_currentErrorIndex++;
		if (m_currentErrorIndex > MAX_ERRORS_COUNT)
			break;
		if (isFolder)
			outStream << "ERR " << m_currentErrorIndex;
		else cout << "ERR " << m_currentErrorIndex;
		int freeSpace = getPrintOffset() - 3 - digitCount(m_errorNumLength);
		if (isFolder)
			outStream << ":";
		else cout << ":";
		for (int i = 0; i < freeSpace; i++)
		{
			if (isFolder) outStream << " ";
			else  cout << " ";
		}
		for (unsigned i = 0; i < (error->getPosition()).getCharNumber(); i++)
		{
			if (isFolder) outStream << " ";
			else cout << " ";
		}
		if (isFolder) outStream << "^ ������ (��� " << error->getCode() << ", [";
		else cout << "^ ������ (��� " << error->getCode() << ", [";
		if (isFolder) outStream << error->getPosition().getLineNumber() + 1 << ", ";
		else cout << error->getPosition().getLineNumber() + 1 << ", ";
		if (isFolder) outStream << error->getPosition().getCharNumber() + 1 << "])" << endl;
		else cout << error->getPosition().getCharNumber() + 1 << "])" << endl;
		for (int i = 0; i < getPrintOffset() - 1; i++)
		{
			if (isFolder) outStream << "*";
			else cout << "*";
		}
		auto errorDescription = m_errorCodes.find(error->getCode());
		if (errorDescription != m_errorCodes.end())
		{
			if (isFolder) outStream << " " << m_errorCodes.find(error->getCode())->second << endl;
			else  cout << " " << m_errorCodes.find(error->getCode())->second << endl;
		}
		else
		{
			if (isFolder) outStream << " ����������� ������" << endl;
			else cout << " ����������� ������" << endl;
		}
	}
}

void InputOutput::nextChar()
{
	m_textPosition->setCharNumber(m_textPosition->getCharNumber() + 1);
	if (m_textPosition->getCharNumber() >= m_currentLineLength)
	{
		if (m_currentChar != '\n')
		{
			m_currentChar = '\n';
			return;
		}	
		m_textPosition->setCharNumber(0);
		m_textPosition->setLineNumber(m_textPosition->getLineNumber() + 1);
		if (m_textPosition->getLineNumber() >= m_programText.size())
		{
			m_currentChar = '\0';
			m_currentLineLength = 0;
			return;
		}
		m_currentLineLength = m_programText[m_textPosition->getLineNumber()].length();
	}
	try
	{
		m_currentChar = (m_programText[m_textPosition->getLineNumber()]).at(m_textPosition->getCharNumber());
	}
	catch (exception e)
	{
		nextChar();
	}

}

char InputOutput::getChar()
{
	return tolower(m_currentChar);
}

TextPosition * InputOutput::getCharPosition()
{
	return m_textPosition;
}

void InputOutput::addError(int errorCode, TextPosition position)
{
	m_errors.push_back(new Error(errorCode, position));
}

void InputOutput::setProgramName(string name)
{
	m_programName = name;
}


void InputOutput::readAllFile(string fileName)
{
	ifstream fileStream(fileName);
	string line;
	while (!fileStream.eof())
	{
		getline(fileStream, line);
		m_programText.push_back(line);
	}
	fileStream.close();
}

void InputOutput::generateErrorCodes()
{
	m_errorCodes = 
	{
		{ 1, "������ � ������� ����" } ,
		{ 2, "������ ���� ���" },
		{ 3, "������ ���� ��������� ����� PROGRAM" },
		{ 4, "������ ���� ������ ')'" },
		{ 5, "������ ���� ������ ':'" },
		{ 6, "����������� ������" },
		{ 7, "������ � ������ ����������" },
		{ 8, "������ ���� OF" },
		{ 9, "������ ���� ������ '('" },
		{ 10, "������ � ����" },
		{ 11, "������ ���� ������ '['" },
		{ 12, "������ ���� ������ ']'" },
		{ 13, "������ ���� ����� END" },
		{ 14, "������ ���� ������ ';'" },
		{ 15, "������ ���� �����" },
		{ 16, "������ ���� ������ '='" },
		{ 17, "������ ���� ����� BEGIN" },
		{ 18, "������ � ������� ��������" },
		{ 19, "������ � ������ �����" },
		{ 20, "������ ���� ������ ','" },
		{ 50, "������ � ���������" },
		{ 51, "������ ���� ������ ':='" },
		{ 52, "������ ���� ����� THEN" },
		{ 70, "������ ���� ������ '+'" },
		{ 71, "������ ���� ������ '-'" },
		{ 53, "������ ���� ����� UNTIL" },
		{ 54, "������ ���� ����� DO" },
		{ 55, "������ ���� ����� TO ��� DOWNTO" },
		{ 56, "������ ���� ����� IF" },
		{ 58, "������ ���� ����� TO ��� DOWNTO" },
		{ 61, "������ ���� ������ '.'" },
		{ 74, "������ ���� ������ '..'" },
		{ 75, "������ � ���������� ���������" },
		{ 76, "������� ������� ��������� ���������" },
		{ 86, "����������� �� ������" },
		{ 100, "������������� ����� �� ������������� ��������" },
		{ 101, "��� ������� ��������" },
		{ 102, "������ ������� ����������� �������" },
		{ 104, "��� �� �������" },
		{ 105, "������������ ����������� �����������" },
		{ 108, "���� ����� ������������ ������" },
		{ 109, "��� �� ������ ���� REAL" },
		{ 111, "��������������� � ����� �������������" },
		{ 112, "������������ ������������ ���" },
		{ 114, "��� ��������� �� ������ ���� REAL ��� INTEGER" },
		{ 115, "���� ������ ���� ���������" },
		{ 116, "������ � ���� ��������� ����������� ���������" },
		{ 117, "������������ ����������� ��������" },
		{ 118, "������������ ��� �p������ ��p������� ����� ������" },
		{ 119, "����������� ��������: ���������� ������ ���������� �� �����������" },
		{ 120, "��� ���������� ������� ������ ���� ���������, ��������� ��� ������������" },
		{ 121, "�������� - �������� �� ����� ���� ������" },
		{ 122, "����������� �������� �������: ��������� ��� ���������� ������" },
		{ 123, "� �������� ������� �������� ��� ����������" },
		{ 124, "F - ������ ������ ��� REAL" },
		{ 125, "������ � ���� ��������� ����������� �������" },
		{ 126, "����� ���������� �� ����������� � ���������" },
		{ 127, "������������ ����������� ����������" },
		{ 128, "��� ���������� ������� �� ������������� ��������" },
		{ 130, "��������� �� ��������� � �������������� ����" },
		{ 131, "�������� ��������� �� ������ �������� �� ��������� 0 .. 255" },
		{ 135, "��� �������� ������ ���� BOOLEAN" },
		{ 137, "������������ ���� ��������� ���������" },
		{ 138, "���������� �� ���� ������" },
		{ 139, "��� ������� �� ������������� ��������" },
		{ 140, "���������� �� ���� ������" },
		{ 141, "���������� ������ ���� ������ ��� �������" },
		{ 142, "������������ ����������� ����������" },
		{ 143, "������������ ��� ��������� �����" },
		{ 144, "������������ ��� ���������" },
		{ 145, "�������� �����" },
		{ 147, "��� ����� �� ��������� � ����� ����������� ���������" },
		{ 149, "��� ������� �� ����� ���� REAL ��� INTEGER" },
		{ 152, "� ���� ������ ��� ������ ����" },
		{ 156, "����� �������� ������������ ��������� ���" },
		{ 165, "����� ������������ ��������� ���" },
		{ 166, "����� ����������� ��������� ���" },
		{ 167, "����������� �����" },
		{ 168, "�������������� �����" },
		{ 169, "������ � ��������� ���������(� ������� ����)" },
		{ 170, "��� �� ����� ���� ��������" },
		{ 177, "����� �� ����������� ������������ ����� �������" },
		{ 182, "���� �� ���������" },
		{ 183, "����������� � ������ ��������� ��������" },
		{ 184, "������� ����� ���� �� ����� ����� ����" },
		{ 186, "�������������� ����� ��� �������� ���������" },
		{ 189, "�������� ����� ����������" },
		{ 190, "��������� ����������� ��������" },
		{ 191, "������ � ������������ ���������" },
		{ 193, "������ ������ ��� ������� � �������� �������" },
		{ 194, "������� ������� ���� �������� ��� ������� � �������� �������" },
		{ 195, "���������� ��������� ��� ������ ���������� ���������" },
		{ 196, "������������ ��� ���������� ���������" },
		{ 197, "��������� ���������(�������) ������ ���� ����������� - ����������" },
		{ 198, "�������������� ���������� ���������� ���������" },
		{ 199, "�������������� ����� ���������� ���������" },
		{ 200, "��� �������� - ������� �� ������������� ��������" },
		{ 201, "������ � ������������ ���������: ������ ���� �����" },
		{ 203, "����� ��������� ��������� ������" },
		{ 204, "������� �� ����" },
		{ 206, "������� ��������� ������������ ���������" },
		{ 207, "������� ������� ������������ ���������" },
		{ 208, "������������ ���� ��������� �������� IN" },
		{ 209, "������ ��������� IN ������ ���� ���������" },
		{ 210, "�������� AND, NOT, OR ������ ���� ��������" },
		{ 211, "������������ ���� ��������� �������� + ��� -" },
		{ 212, "�������� DIV � MOD ������ ���� ������" },
		{ 213, "������������ ���� ��������� �������� *" },
		{ 214, "������������ ���� ��������� �������� /" },
		{ 215, "������ ��������� IN ������ ���� ��������� �������� ���� ���������" },
		{ 216, "����������� �������� ����, ������� ���" },
		{ 305, "��������� �������� ������� �� �������" },
		{ 306, "������������� �������� ������� �� �������" },
		{ 307, "��������� ��� �������� ��������� ������� �� �������" },
		{ 308, "��������� ������� �� ���������� �������" },
	};
}

void InputOutput::showCurrentLine()
{
	if (m_textPosition->getLineNumber() >= m_programText.size())
		return;
	for (int i = 0; i < getPrintOffset(); i++)
		cout << " ";
	cout << m_programText[m_textPosition->getLineNumber()] << endl;
}

int InputOutput::digitCount(int number)
{
	int digits = 0;
	while (number)
	{
		number /= 10;
		digits++;
	}
	return digits;
}

int InputOutput::getPrintOffset()
{
	return m_errorNumLength + 4;
}


