#pragma once
class TextPosition
{
public:
	TextPosition(unsigned lineNumber, unsigned charNumber);
	TextPosition();
	unsigned getLineNumber();
	unsigned getCharNumber();
	void setLineNumber(unsigned lineNumber);
	void setCharNumber(unsigned charNumber);

private:
	unsigned m_lineNumber;
	unsigned m_charNumber;
};

