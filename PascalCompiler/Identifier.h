#pragma once
#include "UseMethod.h"
#include "TypeInformation.h"

using namespace std;

class Identifier
{
public:
	Identifier(string name, UseMethod * useMethod);
	Identifier(string name, UseMethod * useMethod, TypeInformation* typeInfo);
	void setTypeInfo(TypeInformation* type);
	TypeInformation* getTypeInfo();
	string getName();
	UseMethod* getUseMethod();
private:
	string m_name;
	UseMethod* m_useMethod;
	TypeInformation* m_typeInfo;
};

