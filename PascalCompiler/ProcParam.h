#pragma once
#include "UseMethod.h"

class ProcParam :
	public UseMethod
{
public:
	ProcParam(bool isByRef);
	~ProcParam();

	enum TransferType {
		ByValue,
		ByReference
	};
private:
	TransferType m_transferType;
};

