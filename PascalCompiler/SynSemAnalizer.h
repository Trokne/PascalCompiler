#pragma once
#include "Lexer.h"
#include "Scope.h"
#include "EnumType.h"
#include "TypesIdentifier.h"
#include "ProgsIdentifier.h"
#include "ConstsIdentifier.h"
#include "ProcsIdentifier.h"
#include "VarsIdentifier.h"
#include "ScalarType.h"
#include <functional>

class SynSemAnalizer
{
public:
	SynSemAnalizer(Lexer* lexer);
	void start();
private:
	Lexer * m_lexer;

	/* Синтаксический анализатор */
	TypeInformation * execAnalizeFunc(int error, bool isCanBeEmpty, unordered_set<SymType> starters, unordered_set<SymType> followers, function<TypeInformation*(void)> func);
	void execAnalizeFunc(int, bool, unordered_set<SymType>, unordered_set<SymType>, function<void(void)>);
	bool isBelong(SymType, unordered_set<SymType>);
	void skipTo(unordered_set<SymType>);
	void skipTo(unordered_set<SymType>, unordered_set<SymType>);

	string getCurSymbolName();
	void accept(SymType symbol);
	void analizeProgram();
	void analizeBlock();
	void analizeTypePart();
	void analizeVarPart();
	void analizeSameVarDeclaration();
	void analizeProcPart();
	void analizeProcHeader();
	void analizeFormalParamsPart(Identifier*);
	void analizeParamsGroup(bool, Identifier*);
	void analizeCompositeStatement();
	void analizeStatement();
	void analizeForStatement();
	void analizeWhileStatement();
	void analizeRepeatStatement();
	void analizeIfStatement();
	void analizeAssignOperator();
	void analizeProcedureOperator();
	void analizeActualParam(ProcsIdentifier*, int);
	TypeInformation* analizeExpression();
	TypeInformation* analizeSimpleExpression();
	bool analizeSign();
	void checkRightSign(TypeInformation*);
	void checkLogical(TypeInformation*);
	void checkCorrectOperation(TypeInformation*, TypeInformation*, SymType);
	TypeInformation* analizeTerm();
	TypeInformation* analizeFactor();
	bool isAdditiveOperation();
	bool isRelationOperation();
	bool isMultiplicativeOp();
	TypeInformation* analizeType();
	void analizeTypeDefinition();
	
	/* Семантический анализатор */
	ScalarType* m_intType;
	ScalarType* m_charType;
	ScalarType* m_realType;
	ScalarType* m_stringType;
	EnumType* m_booleanType;
	TypeInformation* m_nilType;
	bool m_isOpenedScopeInProc;
	Scope* m_localScope;

	void openScope();
	void closeScope();
	void addIdentifier(Identifier* ident);
	void initializeDummyScope();
	void newVariable(vector<Identifier*>& sameVariables);
};

